## 2.0.0

- The builder now runs when `build_version` is a dependency. `build.yaml`
  changes are no longer required.

## 1.0.1

- Support `package:build_runner` `v1.0.0`.

## 1.0.0

- Initial version, created by Stagehand
